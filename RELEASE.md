# Release Process
1. Rename vNext milestone to vX.Y.Z
2. Create a new vNext milestone
3. Close release milestone
4. If this is a major or minor release, update `dotnet-slnmerge-ci.yml` with a new default version
5. Manually trigger a pipeline with variables:
   - `PKG_VERSION`: X.Y.Z
   - `IS_RELEASE`: true
6. Create a new release
   - Include a link to the milestone
   - Include a link to the nuget
   - Include a list of major changes in release notes